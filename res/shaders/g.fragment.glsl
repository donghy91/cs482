#version 330 core

in vec3 fs_ViewNormal;
out vec4 outColor;

void main()
{
    outColor = vec4( fs_ViewNormal, 1.0f );
};