#version 330 core

in vec3 fs_ViewNormal;
in vec3 fs_ViewPosition;
in vec3 fs_ViewLightPos1;
in vec3 fs_ViewLightPos2;
in vec3 fs_ViewLightPos3;
in vec3 fs_ViewLightPos4;
in vec3 fs_ViewLightPos5;
in vec3 fs_ViewLightPos6;
in vec3 fs_ViewLightPos7;
in vec3 fs_ViewLightPos8;

out vec4 outColor;

uniform int u_vplSetCount;
uniform vec2 u_viewport;
uniform mat4 u_viewMat;
uniform mat4 u_perspMat;
uniform vec3 u_vplIntensity1;
uniform vec3 u_vplDirection1;
uniform int u_numLights1;
uniform vec3 u_vplIntensity2;
uniform vec3 u_vplDirection2;
uniform int u_numLights2;
uniform vec3 u_vplIntensity3;
uniform vec3 u_vplDirection3;
uniform int u_numLights3;
uniform vec3 u_vplIntensity4;
uniform vec3 u_vplDirection4;
uniform int u_numLights4;
uniform vec3 u_vplIntensity5;
uniform vec3 u_vplDirection5;
uniform int u_numLights5;
uniform vec3 u_vplIntensity6;
uniform vec3 u_vplDirection6;
uniform int u_numLights6;
uniform vec3 u_vplIntensity7;
uniform vec3 u_vplDirection7;
uniform int u_numLights7;
uniform vec3 u_vplIntensity8;
uniform vec3 u_vplDirection8;
uniform int u_numLights8;
uniform vec3 u_ambientColor;
uniform vec3 u_diffuseColor;
uniform samplerCube u_shadowTex1;
uniform samplerCube u_shadowTex2;
uniform samplerCube u_shadowTex3;
uniform samplerCube u_shadowTex4;
uniform samplerCube u_shadowTex5;
uniform samplerCube u_shadowTex6;
uniform samplerCube u_shadowTex7;
uniform samplerCube u_shadowTex8;

const float kBias = 0.05f;

void main()
{
	vec2 pos = fs_ViewPosition.xy / fs_ViewPosition.z * u_viewport;
	int index = int(pos.x + pos.y) % u_vplSetCount;

	vec3 fs_ViewLightPos = fs_ViewLightPos1;
	vec3 u_vplDirection = u_vplDirection1;
	int u_numLights = u_numLights1;
	vec3 u_vplIntensity = u_vplIntensity1;
	if (index == 1)
	{
		fs_ViewLightPos = fs_ViewLightPos2;
		u_vplDirection = u_vplDirection2;
		u_numLights = u_numLights2;
		u_vplIntensity = u_vplIntensity2;
	}
	else if(index == 2)
	{
		fs_ViewLightPos = fs_ViewLightPos3;
		u_vplDirection = u_vplDirection3;
		u_numLights = u_numLights3;
		u_vplIntensity = u_vplIntensity3;
	}
	else if(index == 3)
	{
		fs_ViewLightPos = fs_ViewLightPos4;
		u_vplDirection = u_vplDirection4;
		u_numLights = u_numLights4;
		u_vplIntensity = u_vplIntensity4;
	}
	else if(index == 4)
	{
		fs_ViewLightPos = fs_ViewLightPos5;
		u_vplDirection = u_vplDirection5;
		u_numLights = u_numLights5;
		u_vplIntensity = u_vplIntensity5;
	}
	else if(index == 5)
	{
		fs_ViewLightPos = fs_ViewLightPos6;
		u_vplDirection = u_vplDirection6;
		u_numLights = u_numLights6;
		u_vplIntensity = u_vplIntensity6;
	}
	else if(index == 6)
	{
		fs_ViewLightPos = fs_ViewLightPos7;
		u_vplDirection = u_vplDirection7;
		u_numLights = u_numLights7;
		u_vplIntensity = u_vplIntensity7;
	}
	else if(index == 7)
	{
		fs_ViewLightPos = fs_ViewLightPos8;
		u_vplDirection = u_vplDirection8;
		u_numLights = u_numLights8;
		u_vplIntensity = u_vplIntensity8;
	}

    vec3 fragToLight = fs_ViewLightPos - fs_ViewPosition;
    vec3 fragToLightDir = normalize( fragToLight );
    vec4 shadowCubeDir = inverse( u_viewMat ) * vec4( fragToLightDir, 0 );

	float shadowDepth = 0;
	if (index == 0)
	{
		shadowDepth = texture( u_shadowTex1, -shadowCubeDir.xyz ).r;
	}
	else if (index == 1)
	{
		shadowDepth = texture( u_shadowTex2, -shadowCubeDir.xyz ).r;
	}
	else if (index == 2)
	{
		shadowDepth = texture( u_shadowTex3, -shadowCubeDir.xyz ).r;
	}
	else if (index == 3)
	{
		shadowDepth = texture( u_shadowTex4, -shadowCubeDir.xyz ).r;
	}
	else if (index == 4)
	{
		shadowDepth = texture( u_shadowTex5, -shadowCubeDir.xyz ).r;
	}
	else if (index == 5)
	{
		shadowDepth = texture( u_shadowTex6, -shadowCubeDir.xyz ).r;
	}
	else if (index == 6)
	{
		shadowDepth = texture( u_shadowTex7, -shadowCubeDir.xyz ).r;
	}
	else if (index == 7)
	{
		shadowDepth = texture( u_shadowTex8, -shadowCubeDir.xyz ).r;
	}



    if( length( fragToLight ) - kBias > shadowDepth )
    {
        outColor = vec4( 0.f, 0.f, 0.f, 1.f );
        return;
    }
    float decay = clamp( pow( length( fragToLight ), -2.f ), 0.f, 1.f );
    float clampedDiffuseFactor = abs( dot( fs_ViewNormal, fragToLightDir ) );
    float clampedVplCosineFactor = abs( dot( u_vplDirection, fragToLightDir ) );
    vec3 outColor3 = u_vplIntensity * u_diffuseColor * clampedDiffuseFactor * clampedVplCosineFactor * decay;
    outColor3 = clamp( outColor3, 0.f, 1.f / u_numLights ) //clamping total sample contribution for reducing artifacts ("popping")
                + u_ambientColor / u_numLights; //add light source value for handling emittance as well

    //uncomment following code to see where VPLs are located.
    //
    //if (length(fragToLight) < 8.f) {
    //	outColor3.x *= 0.5f;
    //	outColor3.y *= 0.5f;
    //	outColor3.z += 0.5f;
    //}
    //

    outColor = vec4( outColor3, 1.f );
}