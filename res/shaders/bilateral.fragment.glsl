#version 330 core

uniform sampler2D u_Color;
uniform sampler2D u_Normal;
uniform vec2 u_viewport;

in vec2 fs_Texcoord;

out vec4 outColor;

void main()
{
	vec2 pos = fs_Texcoord;
	vec3 centerColor = texture2D( u_Color, pos ).xyz;
	vec3 centerNormal = texture2D( u_Normal, pos ).xyz;

	vec2 d = 1 / u_viewport;
	vec3 colorSum = vec3(0, 0, 0);
	float weightSum = 0;
	int dx = 0;
	int dy = 0;
	int kernelSize = 3;
	for (dx = -kernelSize; dx <= kernelSize; dx++)
		for (dy = -kernelSize; dy <= kernelSize; dy++)
		{
			vec2 samplePos = pos + vec2(d.x * dx, d.y * dy);
			vec3 normal = texture2D( u_Normal, samplePos ).xyz;
			vec3 color = texture2D( u_Color, samplePos ).xyz;
			float weight = 1 - length(centerNormal - normal) * 5;
			weight = clamp(weight, 0, 1);

			weightSum += weight;
			colorSum += color * weight;
		}

    outColor = vec4(colorSum / weightSum, 1.0);
}

