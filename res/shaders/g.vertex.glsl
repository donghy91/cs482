#version 330 core

in  vec3 Position;
in  vec3 Normal;
out vec3 fs_ViewNormal;

uniform mat4 u_modelMat;
uniform mat4 u_viewMat;
uniform mat4 u_perspMat;

void main()
{
	gl_Position = u_perspMat * u_viewMat * u_modelMat * vec4( Position, 1.0 );
    fs_ViewNormal = (u_perspMat * u_viewMat * u_modelMat * vec4( Normal, 0.0 )).xyz;
};