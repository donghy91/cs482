#version 330 core

uniform int u_vplSetCount;
uniform vec2 u_viewport;
uniform mat4 u_modelMat;
uniform mat4 u_viewMat;
uniform mat4 u_perspMat;
uniform vec3 u_vplPosition1;
uniform vec3 u_vplIntensity1;
uniform vec3 u_vplDirection1;
uniform int u_numLights1;
uniform vec3 u_vplPosition2;
uniform vec3 u_vplIntensity2;
uniform vec3 u_vplDirection2;
uniform int u_numLights2;;
uniform vec3 u_vplPosition3;
uniform vec3 u_vplIntensity3;
uniform vec3 u_vplDirection3;
uniform int u_numLights3;
uniform vec3 u_vplPosition4;
uniform vec3 u_vplIntensity4;
uniform vec3 u_vplDirection4;
uniform int u_numLights4;
uniform vec3 u_vplPosition5;
uniform vec3 u_vplIntensity5;
uniform vec3 u_vplDirection5;
uniform int u_numLights5;
uniform vec3 u_vplPosition6;
uniform vec3 u_vplIntensity6;
uniform vec3 u_vplDirection6;
uniform int u_numLights6;
uniform vec3 u_vplPosition7;
uniform vec3 u_vplIntensity7;
uniform vec3 u_vplDirection7;
uniform int u_numLights7;
uniform vec3 u_vplPosition8;
uniform vec3 u_vplIntensity8;
uniform vec3 u_vplDirection8;
uniform int u_numLights8;
uniform vec3 u_ambientColor;
uniform vec3 u_diffuseColor;

in  vec3 Position;
in  vec3 Normal;

out vec3 fs_ViewNormal;
out vec3 fs_ViewPosition;
out vec3 fs_ViewLightPos1;
out vec3 fs_ViewLightPos2;
out vec3 fs_ViewLightPos3;
out vec3 fs_ViewLightPos4;
out vec3 fs_ViewLightPos5;
out vec3 fs_ViewLightPos6;
out vec3 fs_ViewLightPos7;
out vec3 fs_ViewLightPos8;

void main( void )
{
    fs_ViewNormal = ( u_viewMat * u_modelMat * vec4( Normal, 0.0 ) ).xyz;
    fs_ViewPosition = ( u_viewMat * u_modelMat * vec4( Position, 1.0 ) ).xyz;

    fs_ViewLightPos1 = ( u_viewMat * u_modelMat * vec4( u_vplPosition1, 1.0 ) ).xyz;
	fs_ViewLightPos2 = ( u_viewMat * u_modelMat * vec4( u_vplPosition2, 1.0 ) ).xyz;
	fs_ViewLightPos3 = ( u_viewMat * u_modelMat * vec4( u_vplPosition3, 1.0 ) ).xyz;
	fs_ViewLightPos4 = ( u_viewMat * u_modelMat * vec4( u_vplPosition4, 1.0 ) ).xyz;
	fs_ViewLightPos5 = ( u_viewMat * u_modelMat * vec4( u_vplPosition5, 1.0 ) ).xyz;
	fs_ViewLightPos6 = ( u_viewMat * u_modelMat * vec4( u_vplPosition6, 1.0 ) ).xyz;
	fs_ViewLightPos7 = ( u_viewMat * u_modelMat * vec4( u_vplPosition7, 1.0 ) ).xyz;
	fs_ViewLightPos8 = ( u_viewMat * u_modelMat * vec4( u_vplPosition8, 1.0 ) ).xyz;

    gl_Position = u_perspMat * u_viewMat * u_modelMat * vec4( Position, 1.0 );
}