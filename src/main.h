#ifndef _MAIN_H_
#define _MAIN_H_

#include <vector>
#include <cstring>

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <tiny_obj_loader/tiny_obj_loader.h>

#include "gl_snippets.h"

//GL shader/program definitions
GLS_PROGRAM_DEFINE(
    kProgramSceneDraw,
    "res/shaders/screen_draw.vertex.glsl",
    "res/shaders/screen_draw.fragment.glsl",
{ "Position", "Normal" },
{ "outColor" },
{ "u_modelMat", "u_viewMat" , "u_perspMat", "u_vplPosition", "u_vplIntensity", "u_vplDirection", "u_numLights", "u_ambientColor", "u_diffuseColor", "u_shadowTex" }
);

GLS_PROGRAM_DEFINE(
	kProgramSceneSetDraw,
	"res/shaders/screen_set_draw.vertex.glsl",
	"res/shaders/screen_set_draw.fragment.glsl",
	{ "Position", "Normal" },
	{ "outColor" },
	{ "u_vplSetCount", "u_viewport", "u_modelMat", "u_viewMat" , "u_perspMat",
	  "u_vplPosition1", "u_vplIntensity1", "u_vplDirection1", "u_numLights1",
	  "u_vplPosition2", "u_vplIntensity2", "u_vplDirection2", "u_numLights2",
	  "u_vplPosition3", "u_vplIntensity3", "u_vplDirection3", "u_numLights3",
	  "u_vplPosition4", "u_vplIntensity4", "u_vplDirection4", "u_numLights4",
	  "u_vplPosition5", "u_vplIntensity5", "u_vplDirection5", "u_numLights5",
	  "u_vplPosition6", "u_vplIntensity6", "u_vplDirection6", "u_numLights6",
	  "u_vplPosition7", "u_vplIntensity7", "u_vplDirection7", "u_numLights7",
	  "u_vplPosition8", "u_vplIntensity8", "u_vplDirection8", "u_numLights8",
	  "u_ambientColor", "u_diffuseColor",
	  "u_shadowTex1",
	  "u_shadowTex2",
	  "u_shadowTex3",
	  "u_shadowTex4",
	  "u_shadowTex5",
	  "u_shadowTex6",
	  "u_shadowTex7",
	  "u_shadowTex8" }
);

enum program_set_draw_index
{
	ePgmIdxSetCount,
	ePgmIdxViewport,
	ePgmIdxModelTransform,
	ePgmIdxViewTransform,
	ePgmIdxPerspTransform,
	ePgmIdxVplPosition1,
	ePgmIdxVplIntensity1,
	ePgmIdxVplDirection1,
	ePgmIdxVplLightCount1,
	ePgmIdxVplPosition2,
	ePgmIdxVplIntensity2,
	ePgmIdxVplDirection2,
	ePgmIdxVplLightCount2,
	ePgmIdxVplPosition3,
	ePgmIdxVplIntensity3,
	ePgmIdxVplDirection3,
	ePgmIdxVplLightCount3,
	ePgmIdxVplPosition4,
	ePgmIdxVplIntensity4,
	ePgmIdxVplDirection4,
	ePgmIdxVplLightCount4,
	ePgmIdxVplPosition5,
	ePgmIdxVplIntensity5,
	ePgmIdxVplDirection5,
	ePgmIdxVplLightCount5,
	ePgmIdxVplPosition6,
	ePgmIdxVplIntensity6,
	ePgmIdxVplDirection6,
	ePgmIdxVplLightCount6,
	ePgmIdxVplPosition7,
	ePgmIdxVplIntensity7,
	ePgmIdxVplDirection7,
	ePgmIdxVplLightCount7,
	ePgmIdxVplPosition8,
	ePgmIdxVplIntensity8,
	ePgmIdxVplDirection8,
	ePgmIdxVplLightCount8,
	ePgmIdxAmbientColor,
	ePgmIdxDiffuseColor,
	ePgmIdxShadowTex1,
	ePgmIdxShadowTex2,
	ePgmIdxShadowTex3,
	ePgmIdxShadowTex4,
	ePgmIdxShadowTex5,
	ePgmIdxShadowTex6,
	ePgmIdxShadowTex7,
	ePgmIdxShadowTex8
};


GLS_PROGRAM_DEFINE(
    kProgramQuadDraw,
    "res/shaders/quad.vertex.glsl",
    "res/shaders/quad.fragment.glsl",
{ "Position",  "Normal", "Texcoord" },
{ "outColor" },
{ "u_Tex" }
);

GLS_PROGRAM_DEFINE(
    kProgramShadowMapping,
    "res/shaders/shadow.vertex.glsl",
    "res/shaders/shadow.fragment.glsl",
{ "Position" },
{ "outColor" },
{ "u_model", "u_cameraToShadowView" , "u_cameraToShadowProjector" }
);

GLS_PROGRAM_DEFINE(
	kProgramGDraw,
	"res/shaders/g.vertex.glsl",
	"res/shaders/g.fragment.glsl",
	{ "Position", "Normal" },
	{ "outColor" },
	{ "u_modelMat", "u_viewMat" , "u_perspMat" }
);

GLS_PROGRAM_DEFINE(
	kProgramBilateralDraw,
	"res/shaders/quad.vertex.glsl",
	"res/shaders/bilateral.fragment.glsl",
	{ "Position",  "Normal", "Texcoord" },
	{ "outColor" },
	{ "u_Color", "u_Normal", "u_viewport" }
);

constexpr float PI = 3.14159f;
constexpr float kFarPlane = 2000.f;
constexpr float kNearPlane = 0.1f;
constexpr int kShadowSize = 256;
constexpr int kVplCount = 128;

enum gls_program_t
{
    kGlsProgramSceneDraw,
	kGlsProgramSceneSetDraw,
    kGlsProgramQuadDraw,
    kGlsProgramShadowMapping,
	kGlsProgramGDraw,
	kGlsProgramBilateralDraw,
    kGlsProgramMax
};

enum gls_buffer_t
{
    kGlsBufferQuadVertexBuffer,
    kGlsBufferQuadIndexBuffer,
    kGlsBufferMax
};

enum gls_vertex_array_t
{
    kGlsVertexArrayQuad,
    kGlsVertexArrayMax
};

enum gls_framebuffer_t
{
    kGlsFramebufferScreen,
    kGlsFramebufferSceneDraw,
    kGlsFramebufferAccumulate,
	kGlsFramebufferGeometry,
    kGlsFramebufferMax
};

enum gls_texture_t
{
    kGlsTextureScene,
    kGlsTextureAccumulate,
	kGlsTextureGeometry,
    kGlsTextureMax
};

enum render_mode
{
	eRenderModeProject,
	eRenderModeReference,
	eRenderModeSingleSet,

	eRenderModeMax
};

#endif
